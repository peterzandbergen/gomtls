package main

import (
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"golang.org/x/net/http/httpguts"
)

const testCert = `-----BEGIN CERTIFICATE-----
MIIJzDCCB7SgAwIBAgIUC1FIMKaJRs98l8qjrBY9Z20ko34wDQYJKoZIhvcNAQEL
BQAwgYIxCzAJBgNVBAYTAk5MMSAwHgYDVQQKDBdRdW9WYWRpcyBUcnVzdGxpbmsg
Qi5WLjEXMBUGA1UEYQwOTlRSTkwtMzAyMzc0NTkxODA2BgNVBAMML1F1b1ZhZGlz
IFBLSW92ZXJoZWlkIE9yZ2FuaXNhdGllIFNlcnZlciBDQSAtIEczMB4XDTE4MDIy
MTA3NDExNVoXDTIxMDIyMTA3NTEwMFowgbMxHTAbBgNVBAUTFDAwMDAwMDAxODAy
MzI3NDk3MDAwMQswCQYDVQQGEwJOTDETMBEGA1UECAwKR2VsZGVybGFuZDESMBAG
A1UEBwwJQXBlbGRvb3JuMTowOAYDVQQKDDFEaWVuc3Qgdm9vciBoZXQgS2FkYXN0
ZXIgZW4gZGUgb3BlbmJhcmUgcmVnaXN0ZXJzMSAwHgYDVQQDDBdzci1lbmRwb2lu
dC5rYWRhc3Rlci5ubDCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBALrb
vU1z4kM9nBJg4GRrkMhhWMG3lKpZWfZgrHwzXGtmu2F7mTAhsPOtJWr5LfnHvtm0
Cq1hOqunKzWd2qzBG6lHRIw3RNuQOjrxsG0GiWUe0t1TNAJQgD6+E+ZjjS6hvcwy
OocCaRqECcltPYrOfMgf0AjoEPM6jKgEp7x2GxPTT7DGN3SwxZILByltpoGzzJKT
LMFd0Vv8xT70zar4q50MNfc2OqeMnr/V0XcsxISIaFwEsJMtfUL3CLTVj0bwZKDJ
p4Y/vx/HNywohGRbA8ByRhvNh7bDIiuY3ik48Vq5eH+K97DNmAZ+/QZZgkohWB9E
U8q0GS0YlNc57oGgHaECAwEAAaOCBQUwggUBMHsGCCsGAQUFBwEBBG8wbTA8Bggr
BgEFBQcwAoYwaHR0cDovL3RydXN0LnF1b3ZhZGlzZ2xvYmFsLmNvbS9wa2lvc2Vy
dmVyZzMuY3J0MC0GCCsGAQUFBzABhiFodHRwOi8vc2wub2NzcC5xdW92YWRpc2ds
b2JhbC5jb20wHQYDVR0OBBYEFAVaxQQFe1Xl0c7xiUHbYY093zKqMB8GA1UdIwQY
MBaAFLfp0On/Zw7ZnAwHLpfUfkt5ePQgMIIBOgYDVR0gBIIBMTCCAS0wggEfBgpg
hBABh2sBAgUGMIIBDzA0BggrBgEFBQcCARYoaHR0cDovL3d3dy5xdW92YWRpc2ds
b2JhbC5jb20vcmVwb3NpdG9yeTCB1gYIKwYBBQUHAgIwgckMgcZSZWxpYW5jZSBv
biB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFu
Y2Ugb2YgdGhlIHJlbGV2YW50IFF1b1ZhZGlzIENlcnRpZmljYXRpb24gUHJhY3Rp
Y2UgU3RhdGVtZW50IGFuZCBvdGhlciBkb2N1bWVudHMgaW4gdGhlIFF1b1ZhZGlz
IHJlcG9zaXRvcnkgKGh0dHA6Ly93d3cucXVvdmFkaXNnbG9iYWwuY29tKS4wCAYG
Z4EMAQICMD8GA1UdHwQ4MDYwNKAyoDCGLmh0dHA6Ly9jcmwucXVvdmFkaXNnbG9i
YWwuY29tL3BraW9zZXJ2ZXJnMy5jcmwwDgYDVR0PAQH/BAQDAgWgMB0GA1UdJQQW
MBQGCCsGAQUFBwMCBggrBgEFBQcDATAiBgNVHREEGzAZghdzci1lbmRwb2ludC5r
YWRhc3Rlci5ubDCCAm4GCisGAQQB1nkCBAIEggJeBIICWgJYAHYApLkJkLQYWBSH
uxOizGdwCjw1mAT5G9+443fNDsgN3BAAAAFht1hXaAAABAMARzBFAiEA6rImeuMB
nRpsdyjKmz0y+4OoXVxwr8shc1MX/TFe4BkCIFe+5si3T2+6XQYd0v34T5OvCeHK
RGWG0Z2sr70K3E7kAHYAu9nfvB+KcbWTlCOXqpJ7RzhXlQqrUugakJZkNo4e0YUA
AAFht1hXWAAABAMARzBFAiEAwr7ICZhA7AnSpfdbKzsQTU2c8DGeuD8WIUJg34hv
0N8CIDVZkSy2a1xMDvO8/1eI4IrPwhSq/ic71g7p2poF3sUUAHcA7ku9t3XOYLrh
Qmkfq+GeZqMPfl+wctiDAMR7iXqo/csAAAFht1hZLgAABAMASDBGAiEAiN7tGYvn
VDK/uPmmVXQzizwLLsPveeB5X9yTYHcfQvkCIQDe0lBxAyhJUQ+IcTOqaS5ZjewK
1TTtX5hdEf6uNObC+gB2AFWB1MIWkDYBSuoLm1c8U/DA5Dh4cCUIFy+jqh0HE9MM
AAABYbdYWyQAAAQDAEcwRQIhAMgDH1KXeZZ7zG7OHdtWO9mBxKVRjxXTKrG2OY+A
4Be1AiAzB6XDKn+kmSuRL4zIei5NWq7eN53KBCpVnC3ND+bljAB1AG9Tdqwx8DEZ
2JkApFEV/3cVHBHZAsEAKQaNsgiaN9kTAAABYbdYXAcAAAQDAEYwRAIgdFnBJj/y
qlvBiTVrOyvhYtY1IDyhLGKjinGPVXxq+scCID+1WTUgx5PXc8JLB/zpM+EzMOq6
99rPikfJvsHXzjilMA0GCSqGSIb3DQEBCwUAA4ICAQCMK48WJzEhsgWHAa7P87RI
mSt0+UWbxQnU84VFj/dzg7H6fcrTc0rlfHyizWDz7GceUQBHNILz/NzncqIl+y4V
0Hzrk89XsrGbFTLoF1dcmI5iD9txrhQpFQy3AqKrSssY9fyLpG4N593u1NhP0mAX
Nb1OzxHnSmOIzoKbCc/+trud7ryvEiR/EsYchiP2g3JH8ytrypdACHK7wRSoHCvj
llq40xsVzUoWWcA3UlbT8UFGvQ0DlV+vQ4Glm6iaU5JCC5YjBTboXYHmLATonx4H
hYZ5gv+ldkbzy2azKz99zPSdaDkFeDCD0qviDgQjLAtHxtP/tNWaJmps9AABfIdt
dUYf8ImV9zZvNCroCWC1Gh8gX01gyd4/k0B1sLjI7k3vnOzr01ZawcBn5w5QN1kL
JZzbJNVD4DWK+Fn0s/bskoGyISS4pF2wYIO5JUAx12uOggieIcwDIOKoFazv9OzF
JeMHH4oBSBts7jXd3Z5eYBDx4lBkmjLXJb5FxKSpA8Zy0KIFoW0fqBReXIbtZy3V
muAkTKkZEop8kX90Zh6Leh+F5u0XyiK8GfbQ5T0dNCXdzzv1PWX2n86i4AmJmy1n
HFS4iRfKOAfi3ZfnihYu7bxWp44NEJ4zezt1ZOdAK/PW2gqqqWeOEo+Hgses+UsX
mflwcRpateTzwGrUsfcgjg==
-----END CERTIFICATE-----`

const largeValue = "MIIJzDCCB7SgAwIBAgIUC1FIMKaJRs98l8qjrBY9Z20ko34wDQYJKoZIhvcNAQEL\r\n" +
	" BQAwgYIxCzAJBgNVBAYTAk5MMSAwHgYDVQQKDBdRdW9WYWRpcyBUcnVzdGxpbmsg\r\n" +
	" Qi5WLjEXMBUGA1UEYQwOTlRSTkwtMzAyMzc0NTkxODA2BgNVBAMML1F1b1ZhZGlz\r\n" +
	" IFBLSW92ZXJoZWlkIE9yZ2FuaXNhdGllIFNlcnZlciBDQSAtIEczMB4XDTE4MDIy\r\n"

func escapeHeaderValue(s string) string {
	r := strings.NewReplacer("\r\n", "\r", "\n", " ")
	res := r.Replace(s)
	return res
}

const smallValue = `dit zijn
meerdere regels
Hallo`


func pemHeader(s string) string {
	r := strings.NewReplacer("----- ", "-----\n", " -----", "\n-----")
	res := r.Replace(s)
	return res
}

func TestEscapeToPem(t *testing.T) {
	esc := escapeHeaderValue(testCert)
	ph := pemHeader(esc)
	b, _ := pem.Decode([]byte(ph))
	cert, err := x509.ParseCertificate(b.Bytes)
	if err != nil {
		t.Errorf("error parsing x509 cert: %s", err)
	}
	sub := cert.Subject.String()
	t.Logf("%s", sub)
}

func TestValidValue(t *testing.T) {
	e := escapeHeaderValue(smallValue)
	if !httpguts.ValidHeaderFieldValue(e) {
		t.Errorf("valid \"%s\" not valid", smallValue)
	}
}

func TestLargeMultiLineHeader(t *testing.T) {
	const mhkey = "X-Multi-Line-Test"
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cert := r.Header.Get(mhkey)
		if len(cert) == 0 {
			http.Error(w, "missing multi line header", http.StatusBadRequest)
		}
		// split first an last line
		fmt.Fprintln(w, "Hello, client")
	}))

	u := ts.URL
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		t.Errorf("error creating request: %s", err)
	}
	req.Header.Add(mhkey, url.QueryEscape(largeValue))
	client := &http.Client{}
	_, err = client.Do(req)
	if err != nil {
		t.Errorf("error sending request: %s", err)
	}
	// val := resp.Header.Values(mhkey)
	// if val[0] != largeValue {
	// 	t.Errorf("error, expected:\n\n%s\n\n received:\n\n%s\n", largeValue, val[0])
	// }
}

func TestMultiLineHeader(t *testing.T) {
	const mhkey = "X-Multi-Line-Test"

	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cert := r.Header.Get(mhkey)
		if len(cert) == 0 {
			http.Error(w, "missing multi line header", http.StatusBadRequest)
		}
		fmt.Fprintln(w, "Hello, client")
	}))

	u := ts.URL
	req, err := http.NewRequest("GET", u, nil)
	if err != nil {
		t.Errorf("error creating request: %s", err)
	}
	req.Header.Add(mhkey, escapeHeaderValue(smallValue))
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		t.Errorf("error sending request: %s", err)
	}
	val := resp.Header.Values(mhkey)
	if val[0] != smallValue {
		t.Errorf("error, expected:\n\n%s\n\n received:\n\n%s\n", smallValue, val[0])
	}
}
