package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
)

var (
	ErrNoTLS      = errors.New("no tls in response")
	ErrNoEndpoint = errors.New("no endpoint argument given")

	probe func(endpoint string) (*tls.ConnectionState, error)
)

func probeRoundtrip(endpoint string) (*tls.ConnectionState, error) {
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		return nil, err
	}
	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		return nil, err
	}
	if resp.TLS == nil {
		return nil, ErrNoTLS
	}
	return resp.TLS, nil
}

func probeTLS(endpoint string) (*tls.ConnectionState, error) {
	resp, err := http.Get(endpoint)
	if err != nil {
		return nil, err
	}
	if resp.TLS == nil {
		return nil, ErrNoTLS
	}
	return resp.TLS, nil
}

func printErrorf(fs string, args ...any) {
	fmt.Fprintf(os.Stderr, fs+"\n", args...)
}

func printCerts(w io.Writer, certs []*x509.Certificate, opts *options) error {

	for i, c := range certs {
		fmt.Fprintf(w, "Cert %d\n", i)
		printCert(w, c, opts)
	}
	return nil
}

func printCert(w io.Writer, cert *x509.Certificate, opts *options) error {
	fe := func(data []byte) string {
		res, _ := pemEncode(data, "CERTIFICATE")
		return string(res)
	}
	fmt.Fprintf(w, "    Subject: %s\n    Issuer:  %s\n", cert.Subject.String(), cert.Issuer.String())
	if !opts.noout {
		fmt.Fprintf(w, "%s", fe(cert.Raw))
	}
	return nil
}

func pemEncode(data []byte, ptype string) ([]byte, error) {
	block := pem.Block{
		Type:  "CERTIFICATE",
		Bytes: data,
	}
	return pem.EncodeToMemory(&block), nil
}

func run(opts *options) error {
	probe = probeRoundtrip
	if opts.follow {
		probe = probeTLS
	}

	tls, err := probe(opts.args[0])
	if err != nil {
		return err
	}
	if opts.n < 0 {
		// print the chain.
		printCerts(os.Stdout, tls.VerifiedChains[0], opts)
		return nil
	}
	if opts.n < len(tls.VerifiedChains[0]) {
		printCert(os.Stdout, tls.VerifiedChains[0][opts.n], opts)
	}

	return nil
}

type options struct {
	args   []string
	n      int
	follow bool
	noout  bool
}

func getOptions(args ...string) (*options, error) {
	fs := flag.NewFlagSet(args[0], flag.ExitOnError)
	n := fs.Uint("n", 1000, "when set only print the nth cert, 0 being the first cert")
	noout := fs.Bool("noout", false, "set to not print the pem")
	follow := fs.Bool("follow", false, "set to follow redirects")
	if err := fs.Parse(args[1:]); err != nil {
		return nil, err
	}
	if fs.NArg() < 1 {
		return nil, ErrNoEndpoint
	}
	opts := &options{
		args:   fs.Args(),
		follow: *follow,
		noout:  *noout,
		n:      -1,
	}
	if *n < 1000 {
		opts.n = int(*n)
	}
	return opts, nil
}

func main() {
	opts, err := getOptions(os.Args...)

	if err != nil {
		printErrorf("%s", err.Error())
		os.Exit(1)
	}

	if err := run(opts); err != nil {
		printErrorf("run error: %s", err.Error())
	}
}
