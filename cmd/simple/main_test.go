package main

import (
	"testing"
)

func Test_probeTLS(t *testing.T) {
	type args struct {
		endpoint string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "google.com",
			args:    args{endpoint: "https://www.google.com"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := probeTLS(tt.args.endpoint)
			if (err != nil) != tt.wantErr {
				t.Errorf("probeTLS() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("found TLS, %s", got.ServerName)
		})
	}
}

func TestRun(t *testing.T) {
	tests := []struct{
		name string
		args []string
	}{
		// {
		// 	name: "show all",
		// 	args: []string{"simple", "https://www.google.com"},
		// },
		{
			name: "show first",
			args: []string{"simple", "-n", "0", "https://www.google.com"},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			opts, err := getOptions(tt.args...)
			if err != nil {
				t.Fatalf("getOptions error: %s", err.Error())
			}
			err = run(opts)
			if err != nil {
				t.Fatalf("run error: %s", err.Error())
			}
		})
	}
}