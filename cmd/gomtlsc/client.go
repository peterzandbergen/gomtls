// client/client.go

package main

import (
	"bytes"
	"context"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"time"
)

var config struct {
	// addr is the remote addres to communicate with
	url     string
	tcp     string
	key     string
	cert    string
	cacerts []string
	tlsver  uint16
	level   string
	nobody  bool
	auth    tls.ClientAuthType
	data    string
}

func parseConfig() {
	c := &config
	flag.StringVar(&c.key, "key", "", "server key in pem format")
	flag.StringVar(&c.cert, "cert", "", "server certificate in pem format")
	flag.StringVar(&c.level, "level", "https", "osi level, tcps or https")
	flag.StringVar(&c.tcp, "tcp", "dual", "type of tcp to use: 4, 6 or dual")
	flag.StringVar(&c.data, "data", "", "data to send in POST")
	flag.BoolVar(&c.nobody, "nobody", false, "set to hide body")

	// Indirect values.
	// TLS version.
	var tv int
	flag.IntVar(&tv, "tlsver", 2, "minimal version of tls, 1 for 1.1, 2 for 1.2, 3 for 1.3")

	// CA Certificates.
	var cs string
	flag.StringVar(&cs, "cacerts", "", "list of root ca cert files separated by ','")

	flag.Parse()

	// TCP version
	switch c.tcp {
	case "6", "4":
		c.tcp = "tcp" + c.tcp
	default:
		config.tcp = "tcp"
	}

	// Remaining parameter is the url to get.
	if len(flag.CommandLine.Args()) > 0 {
		c.url = flag.Arg(0)
	}

	// Get the ca cert files.
	if len(cs) > 0 {
		c.cacerts = strings.Split(cs, ",")
	}

	// TLS version.
	switch tv {
	case 1:
		c.tlsver = tls.VersionTLS11
	case 2:
		c.tlsver = tls.VersionTLS12
	case 3:
		c.tlsver = tls.VersionTLS13
	}
}

func loadCertFiles(certPool *x509.CertPool, files []string) error {
	if files == nil {
		return nil
	}
	for _, f := range files {
		// load CA certificate file and add it to list of client CAs
		caCertFile, err := ioutil.ReadFile(f)
		if err != nil {
			return fmt.Errorf("error reading %s: %w", f, err)
		}
		certPool.AppendCertsFromPEM(caCertFile)
	}
	return nil
}

// logCertificates logs the acceptableCAs for client certification
func logCertificates(acceptableCAs [][]byte) {
	log.Printf("CA Names offered by server")
	for _, ca := range acceptableCAs {
		var name pkix.RDNSequence
		if _, err := asn1.Unmarshal(ca, &name); err == nil {
			log.Printf("  %s", name.String())
		} else {
			log.Printf("error unmarshalling name: %s", err)
		}
	}
}

//
func newGetClientCertificate(certs []tls.Certificate, setAuth func(bool)) func(*tls.CertificateRequestInfo) (*tls.Certificate, error) {
	return func(requestInfo *tls.CertificateRequestInfo) (*tls.Certificate, error) {
		// For now return the cert.
		logCertificates(requestInfo.AcceptableCAs)
		setAuth(false)
		log.Printf("Client cert requested by server")
		var err error
		for _, c := range certs {
			if err = requestInfo.SupportsCertificate(&c); err == nil {
				var cert *x509.Certificate
				if c.Leaf != nil {
					cert = c.Leaf
				} else {
					cert, _ = x509.ParseCertificate(c.Certificate[0])
				}
				log.Printf("Client cert accepted")
				log.Printf("  Subject: %s", cert.Subject.String())
				log.Printf("  Issuer:  %s", cert.Issuer.String())
				setAuth(true)
				return &c, nil
			}
			err = fmt.Errorf("cert not supported: %w", err)
		}
		log.Print("Could not find suitable client cert for authentication")
		return nil, err
	}
}

func newTLSConfig(setAuth func(bool)) *tls.Config {
	certPool := x509.NewCertPool()
	err := loadCertFiles(certPool, config.cacerts)
	if err != nil {
		log.Fatalf("%s", err)
	}
	cert, err := tls.LoadX509KeyPair(config.cert, config.key)
	if err != nil {
		log.Fatalf("error loading key pair: %s", err)
	}

	tlsConfig := &tls.Config{
		// Certificates:  []tls.Certificate{cert},
		RootCAs:              certPool,
		MinVersion:           config.tlsver,
		Renegotiation:        tls.RenegotiateOnceAsClient,
		GetClientCertificate: newGetClientCertificate([]tls.Certificate{cert}, setAuth),
	}
	return tlsConfig
}

func logHTTPInfo(resp *http.Response) {
	hostname, _ := os.Hostname()
	log.Print("Client Hostname: ", hostname)

	log.Print("Client IP Addresses: ")
	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			log.Print("  IP: ", ip)
		}
	}

	var b bytes.Buffer
	fmt.Fprintf(&b, "Status: %s\n", resp.Status)
	b.WriteString("HTTP Headers:\n")
	resp.Header.Write(&b)
	b.WriteRune('\n')
	if !config.nobody {
		fmt.Fprintln(&b, "HTTP Body:")
		io.Copy(&b, resp.Body)
	}
	// resp.Write(&b)
	log.Print(b.String())
}

func logCertSubjectIssuer(i int, cert *x509.Certificate) {
	log.Printf("  %d Subject: %s", i, cert.Subject.String())
	log.Printf("    Issuer:  %s", cert.Issuer.String())
}

func logConnState(state *tls.ConnectionState) {
	log.Print(">>>>>>>>>>>>>>>> State <<<<<<<<<<<<<<<<")
	log.Printf("Version: %x", state.Version)
	log.Printf("HandshakeComplete: %t", state.HandshakeComplete)
	log.Printf("DidResume: %t", state.DidResume)
	log.Printf("CipherSuite: %x", state.CipherSuite)
	log.Printf("NegotiatedProtocol: %s", state.NegotiatedProtocol)
	log.Printf("NegotiatedProtocolIsMutual: %t", state.NegotiatedProtocolIsMutual)

	log.Print("Certificate chain:")
	for i, cert := range state.PeerCertificates {
		logCertSubjectIssuer(i, cert)
	}
	log.Print("Verified Chains:")
	for i, certs := range state.VerifiedChains {
		log.Printf("Verified Chain %d:", i)
		for j, cert := range certs {
			logCertSubjectIssuer(j, cert)
		}
	}
	log.Print(">>>>>>>>>>>>>>>> State End <<<<<<<<<<<<<<<<")
}

type client struct {
	http.Client
	clientAutenticated bool
}

func (c *client) SetClientAutenticated(auth bool) {
	c.clientAutenticated = auth
}

func networkDialContext(forceNetwork string) func(ctx context.Context, network, addr string) (net.Conn, error) {
	d := &net.Dialer{
		Timeout:   30 * time.Second,
		KeepAlive: 30 * time.Second,
		DualStack: false,
	}
	return func(ctx context.Context, n, addr string) (net.Conn, error) {
		return d.DialContext(ctx, forceNetwork, addr)
	}
}

func newRequest() (*http.Request, error) {
	if len(config.data) > 0 {
		b := bytes.NewBufferString(config.data)
		return http.NewRequest("POST", config.url, b)
	}
	return http.NewRequest("GET", config.url, nil)
}

func newClient() *client {
	c := &client{}
	if len(config.cert) > 0 && len(config.key) > 0 {
		tlsConfig := newTLSConfig(c.SetClientAutenticated)

		c.Client = http.Client{
			Timeout: time.Minute * 3,
			Transport: &http.Transport{
				TLSClientConfig: tlsConfig,
				DialContext:     networkDialContext(config.tcp),
			},
		}
	}
	return c
}

func main() {
	parseConfig()
	client := newClient()

	client.SetClientAutenticated(false)
	// resp, err := client.Get(config.url)
	req, err := newRequest()
	resp, err := client.Do(req)
	if err != nil {
		log.Fatalf("Error making get request: %v", err)
		return
	}

	if client.clientAutenticated {
		log.Print("Client was authenticated")
	} else {
		log.Print("Client was NOT authenticated")
	}
	logHTTPInfo(resp)
	if resp.TLS == nil {
		log.Printf("no tls for host %s", config.url)
	} else {
		logConnState(resp.TLS)
	}
}
