// server/server.go

package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
)

func logHTTPInfo(req *http.Request) {
	hostname, _ := os.Hostname()
	log.Print("Hostname:", hostname)

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			log.Print("IP:", ip)
		}
	}

	log.Print("RemoteAddr:", req.RemoteAddr)
	var b bytes.Buffer
	b.WriteString("HTTP Headers\n")
	req.Write(&b)
	log.Print(b.String())
}

func logConnection(c net.Conn) {
	hostname, _ := os.Hostname()
	log.Print("Hostname:", hostname)

	ifaces, _ := net.Interfaces()
	for _, i := range ifaces {
		addrs, _ := i.Addrs()
		// handle err
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			log.Print("IP:", ip)
		}
	}

	log.Print("RemoteAddr:", c.RemoteAddr())
}

var config struct {
	addr    string
	network string
	key     string
	cert    string
	cacerts []string
	tlsver  uint16
	level   string
	auth    tls.ClientAuthType
}

func parseConfig() {
	c := &config
	flag.StringVar(&c.addr, "addr", ":8888", "listen address and port")
	flag.StringVar(&c.key, "key", "", "server key in pem format")
	flag.StringVar(&c.cert, "cert", "", "server certificate in pem format")
	flag.StringVar(&c.level, "level", "https", "osi level, tcps or https")
	flag.StringVar(&c.network, "network", "tcp", "type of network to use: tcp, tcp4, tcp6")

	var tv int
	flag.IntVar(&tv, "tlsver", 2, "minimal version of tls, 1 for 1.1, 2 for 1.2, 3 for 1.3")

	var cs string
	flag.StringVar(&cs, "cacerts", "", "list of client ca cert files separated by ','")

	var ca string
	flag.StringVar(&ca, "clientauth", "verifyifgiven", "client authentication [no | request | requireany | verifyifgiven | requireverify")

	flag.Parse()
	if len(cs) > 0 {
		c.cacerts = strings.Split(cs, ",")
	}

	switch tv {
	case 1:
		c.tlsver = tls.VersionTLS11
	case 2:
		c.tlsver = tls.VersionTLS12
	case 3:
		c.tlsver = tls.VersionTLS13
	}

	switch ca {
	case "no":
		c.auth = tls.NoClientCert
	case "request":
		c.auth = tls.RequestClientCert
	case "requireany":
		c.auth = tls.RequireAnyClientCert
	case "verifyifgiven":
		c.auth = tls.VerifyClientCertIfGiven
	case "requireverify":
		c.auth = tls.RequireAndVerifyClientCert
	}
}

func logConnState(state *tls.ConnectionState) {
	log.Print(">>>>>>>>>>>>>>>> State <<<<<<<<<<<<<<<<")
	log.Printf("Version: %x", state.Version)
	log.Printf("HandshakeComplete: %t", state.HandshakeComplete)
	log.Printf("DidResume: %t", state.DidResume)
	log.Printf("CipherSuite: %x", state.CipherSuite)
	log.Printf("NegotiatedProtocol: %s", state.NegotiatedProtocol)
	log.Printf("NegotiatedProtocolIsMutual: %t", state.NegotiatedProtocolIsMutual)

	log.Print("Certificate chain:")
	for i, cert := range state.PeerCertificates {
		subject := cert.Subject
		issuer := cert.Issuer
		log.Printf(" %d s:/C=%v/ST=%v/L=%v/O=%v/OU=%v/CN=%s", i, subject.Country, subject.Province, subject.Locality, subject.Organization, subject.OrganizationalUnit, subject.CommonName)
		log.Printf("   i:/C=%v/ST=%v/L=%v/O=%v/OU=%v/CN=%s", issuer.Country, issuer.Province, issuer.Locality, issuer.Organization, issuer.OrganizationalUnit, issuer.CommonName)
	}
	log.Print("Verified Chains:")
	for i, certs := range state.VerifiedChains {
		log.Printf("Verified Chain %d:", i)
		for j, cert := range certs {
			subject := cert.Subject
			issuer := cert.Issuer
			log.Printf(" %d s:/C=%v/ST=%v/L=%v/O=%v/OU=%v/CN=%s", j, subject.Country, subject.Province, subject.Locality, subject.Organization, subject.OrganizationalUnit, subject.CommonName)
			log.Printf("   i:/C=%v/ST=%v/L=%v/O=%v/OU=%v/CN=%s", issuer.Country, issuer.Province, issuer.Locality, issuer.Organization, issuer.OrganizationalUnit, issuer.CommonName)
		}
	}
	log.Print(">>>>>>>>>>>>>>>> State End <<<<<<<<<<<<<<<<")
}

func logTLSInfo(request *http.Request) {
	if request.TLS == nil {
		log.Printf("no TLS for for %s", request.Host)
	}
	logConnState(request.TLS)
}

func loadCertFiles(certPool *x509.CertPool, files []string) error {
	if files == nil {
		return nil
	}
	for _, f := range files {
		// load CA certificate file and add it to list of client CAs
		caCertFile, err := ioutil.ReadFile(f)
		if err != nil {
			return fmt.Errorf("error reading %s: %w", f, err)
		}
		certPool.AppendCertsFromPEM(caCertFile)
	}
	return nil
}

func handleAccept(c net.Conn) {
	ctls, ok := c.(*tls.Conn)
	// defer ctls.Close()
	if !ok {
		log.Print("accept error, no tls connection")
		return
	}
	cs := ctls.ConnectionState()
	log.Print("====================== BEGIN ACCEPT =======================")
	logConnection(ctls)
	logConnState(&cs)
	log.Print("====================== END ACCEPT   =======================")
}

func listenAndServeTCPS(tlsConfig *tls.Config) {
	log.Printf("serving TCPS on %s", config.addr)
	conn, err := tls.Listen(config.network, config.addr, tlsConfig)
	if err != nil {
		log.Fatalf("error listening on TCPS: %s", err.Error())
	}
	for {
		c, err := conn.Accept()
		if err != nil {
			log.Printf("accept error: %s", err.Error())
		}
		handleAccept(c)
	}
}

func listenAndServeHTTPS(tlsConfig *tls.Config) {
	// set up handler to listen to root path
	handler := http.NewServeMux()
	handler.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		log.Print("====================== BEGIN HTTP Request =======================")
		logHTTPInfo(request)
		logTLSInfo(request)
		fmt.Fprintf(writer, "hello world \n")
		log.Print("====================== END HTTP Request =========================")
	})

	// serve on port 9090 of local host
	server := http.Server{
		Addr:      config.addr,
		Handler:   handler,
		TLSConfig: tlsConfig,
	}

	log.Printf("serving HTTPS on %s", config.addr)
	if err := server.ListenAndServeTLS(config.cert, config.key); err != nil {
		log.Fatalf("error listening to port: %v", err)
	}
}

func buildTLSConfig() *tls.Config {
	certPool := x509.NewCertPool()
	err := loadCertFiles(certPool, config.cacerts)
	if err != nil {
		log.Fatalf("%s", err)
	}
	cert, err := tls.LoadX509KeyPair(config.cert, config.key)
	if err != nil {
		log.Fatalf("error loading key pair: %s", err)
	}

	// Get the

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		ClientAuth: config.auth,
		ClientCAs:  certPool,
		MinVersion: config.tlsver,
	}
	return tlsConfig
}

func main() {
	parseConfig()
	if len(config.cert) == 0 || len(config.key) == 0 {
		log.Fatalf("-cert and -key are mandatory")
	}

	tlsConfig := buildTLSConfig()

	if config.level == "https" {
		listenAndServeHTTPS(tlsConfig)
	}
	if config.level == "tcps" {
		listenAndServeTCPS(tlsConfig)
	}

}
