# Test commands

## Curl tests

HTTP GET met client certs
```
curl -v --cert ebms.cbs.nl.cert.pem \
    --key ebms.cbs.nl.key.pem 
    --anyauth \
    'https://ebms.cbs.nl/ebms_test'
```

HTTP GET met client certs en CA chain voor controle
```
curl -v --cert ebms.cbs.nl.cert.pem --key ebms.cbs.nl.key.pem \
    --cacert ebms.cbs.nl.ca-chain.pem  
    'https://ebms.cbs.nl/ebms_test'
```

HTTP GET met CA-chain voor ebms.cbs.nl
```
curl -v --cert ebms.cbs.nl.cert.pem \
    --key ebms.cbs.nl.key.pem \
    --cacert ebms.cbs.nl.ca-chain.pem  \
    'https://ebms.cbs.nl/ebms_test'
```

HTTP GET met eigen CA-chain voor ebms.cbs.nl/ebms_test
```
curl -v --cert ebms.cbs.nl.cert.pem --key ebms.cbs.nl.key.pem \
    --cacert '/c/Users/BIM UitleenPool/DevProjects/gomtls/ca/ca.crt'  \
    'https://ebms.cbs.nl/ebms_test'
```

HTTP GET tegen local host
```
curl -v --cert ca/client.crt --key ca/client.key --cacert 'ca/ca.crt'  \
    'https://localhost:9090'
```

```
curl -v --cert ebms.cbs.nl.cert.pem \
    --key ebms.cbs.nl.key.pem \
    --cacert ebms.cbs.nl.ca-chain.pem  \
    'https://ebms.cbs.nl/ebms_test'
```

```
curl -v --cert ebms.cbs.nl.cert.pem \
    --key ebms.cbs.nl.key.pem \
    --cacert '/c/Users/BIM UitleenPool/DevProjects/gomtls/ca/ca.crt' \
    'https://ebms.cbs.nl/ebms_test'
```

```
curl -v --cert ca/client.crt \
    --key ca/client.key \
    --cacert 'ca/ca.crt'  
    'https://localhost:9090'
```


## OpenSSL s_client

verzamel certs van de server
```
openssl s_client -host ebms.cbs.nl -port 443 -prexit -showcerts
```

Extra opties voor debugging
```
 -security_debug            Enable security debug messages
 -security_debug_verbose    Output more security debug output
```

## Test Server

```
go run cmd/server/server.go -key ca/server.localhost.key -cert ca/server.localhost.crt -cacerts ca/ca.crt -addr ":9090"
```

Test de local server en specificeer de certs om server mee te testen.
```
curl -v  'https://localhost:9090' --cacert 'ca/ca.crt'
```

```
curl -v --cert ebms.cbs.nl.cert.pem --key ebms.cbs.nl.key.pem --anyauth --cacert   'https://ebms.cbs.nl/ebms_test'


curl -v --cert ebms.cbs.nl.cert.pem --key ebms.cbs.nl.key.pem --cacert ebms.cbs.nl.ca-chain.pem  'https://ebms.cbs.nl/ebms_test'

verzamel certs:
openssl s_client -host ebms.cbs.nl -port 443 -prexit -showcerts

 -security_debug            Enable security debug messages
 -security_debug_verbose    Output more security debug output


go run cmd/server/server.go -key ca/server.localhost.key -cert ca/server.localhost.crt -cacerts ca/ca.crt -addr ":9090"

curl -v  'https://localhost:9090' --cacert 'ca/ca.crt'



 0 s:serialNumber = 00000001802327497000, C = NL, ST = Gelderland, L = Apeldoorn, O = Dienst voor het Kadaster en de openbare registers, CN = service30.kadaster.nl
   i:C = NL, O = QuoVadis Trustlink B.V., organizationIdentifier = NTRNL-30237459, CN = QuoVadis PKIoverheid Organisatie Server CA - G3
 1 s:C = NL, O = QuoVadis Trustlink B.V., organizationIdentifier = NTRNL-30237459, CN = QuoVadis PKIoverheid Organisatie Server CA - G3
   i:C = NL, O = Staat der Nederlanden, CN = Staat der Nederlanden Organisatie Services CA - G3
 2 s:C = NL, O = Staat der Nederlanden, CN = Staat der Nederlanden Organisatie Services CA - G3
   i:C = NL, O = Staat der Nederlanden, CN = Staat der Nederlanden Root CA - G3
 3 s:C = NL, O = Staat der Nederlanden, CN = Staat der Nederlanden Root CA - G3
   i:C = NL, O = Staat der Nederlanden, CN = Staat der Nederlanden Root CA - G3 => RootCA-G3.pem



go run cmd/client/client.go -key ~/Documents/EbMS/ebms.cbs.nl.key.pem -cert ~/Documents/EbMS/ebms.cbs.nl.cert-chain.pem -cacerts ~/Documents/EbMS/ebms.cbs.nl.ca-chain.pem 'https://ebms.cbs.nl/ebms_test'

go run cmd/client/client.go -cacerts ~/Documents/EbMS/ebms.cbs.nl.ca-chain.pem 'https://ebms.cbs.nl/ebms_test'


# Kadaster sr-endpoint
$ openssl x509 -text -noout -in 2936_client_pubkey.pem
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            0b:51:48:30:a6:89:46:cf:7c:97:ca:a3:ac:16:3d:67:6d:24:a3:7e
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = NL, O = QuoVadis Trustlink B.V., organizationIdentifier = NTRNL-30237459, CN = QuoVadis PKIoverheid Organisatie Server CA - G3
        Validity
            Not Before: Feb 21 07:41:15 2018 GMT
            Not After : Feb 21 07:51:00 2021 GMT
        Subject: serialNumber = 00000001802327497000, C = NL, ST = Gelderland, L = Apeldoorn, O = Dienst voor het Kadaster en de openbare registers, CN = sr-endpoint.kadaster.nl

CN=QuoVadis PKIoverheid Organisatie Server CA - G3,2.5.4.97=#130e4e54524e4c2d3330323337343539,O=QuoVadis Trustlink B.V.,C=NL

service30
2020/04/16 22:40:34    CN=DigiCert SHA2 Secure Server CA,O=DigiCert Inc,C=US
2020/04/16 22:40:34    CN=AddTrust External CA Root,OU=AddTrust External TTP Network,O=AddTrust AB,C=SE
2020/04/16 22:40:34    CN=Microsoft IT TLS CA 4,OU=Microsoft IT,O=Microsoft Corporation,L=Redmond,ST=Washington,C=US
2020/04/16 22:40:34    CN=QuoVadis Root CA 2,O=QuoVadis Limited,C=BM
2020/04/16 22:40:34    CN=Entrust Certification Authority - L1K,OU=(c) 2012 Entrust\, Inc. - for authorized use only,OU=See www.entrust.net/legal-terms,O=Entrust\, Inc.,C=US
2020/04/16 22:40:34    CN=Staat der Nederlanden EV Intermediair CA,O=Staat der Nederlanden,C=NL
2020/04/16 22:40:34    CN=KPN PKIoverheid EV CA,O=KPN B.V.,C=NL
2020/04/16 22:40:34    CN=HydrantID SSL ICA G2,O=HydrantID (Avalanche Cloud Corporation),C=US
2020/04/16 22:40:34    CN=QuoVadis PKIoverheid EV CA,O=QuoVadis Trustlink BV,C=NL
2020/04/16 22:40:34    CN=KPN CM PKIoverheid EV CA,O=KPN Corporate Market B.V.,C=NL
2020/04/16 22:40:34    CN=Sectigo RSA Organization Validation Secure Server CA,O=Sectigo Limited,L=Salford,ST=Greater Manchester,C=GB
2020/04/16 22:40:34    CN=KPN PKIoverheid Organisatie Server CA - G3,O=KPN Corporate Market B.V.,C=NL
2020/04/16 22:40:34    CN=KPN BV PKIoverheid Organisatie Server CA - G3,2.5.4.97=#130e4e54524e4c2d3237313234373031,O=KPN B.V.,C=NL
2020/04/16 22:40:34    CN=Digidentity BV PKIoverheid Organisatie Server CA - G3,2.5.4.97=#130e4e54524e4c2d3237333232363331,O=Digidentity B.V.,C=NL
2020/04/16 22:40:34    CN=QuoVadis PKIoverheid Organisatie Server CA - G3,2.5.4.97=#130e4e54524e4c2d3330323337343539,O=QuoVadis Trustlink B.V.,C=NL
2020/04/16 22:40:34    CN=IdentityValidation-GAA.kadaster.nl,O=Dienst voor het Kadaster en de openbare registers,L=Apeldoorn,ST=Gelderland,C=NL


embs.cbs.nl
2020/04/16 22:41:00    CN=Staat der Nederlanden Organisatie Services CA - G3,O=Staat der Nederlanden,C=NL
2020/04/16 22:41:00    CN=QuoVadis PKIoverheid Organisatie Server CA - G3,2.5.4.97=#130e4e54524e4c2d3330323337343539,O=QuoVadis Trustlink B.V.,C=NL
2020/04/16 22:41:00    CN=Staat der Nederlanden Root CA - G3,O=Staat der Nederlanden,C=NL
Accepted: 
2020/04/16 22:41:00    s:/C=[NL]/ST=[Zuid-Holland]/L=['s-Gravenhage]/O=[Centraal Bureau voor de Statistiek]/OU=[]/CN=ebms.cbs.nl
2020/04/16 22:41:00    i:/C=[NL]/ST=[]/L=[]/O=[QuoVadis Trustlink B.V.]/OU=[]/CN=QuoVadis PKIoverheid Organisatie Server CA - G3

Nodig voor kadaster sr-endpoint (uit sr-endpoint certificaat 2936_client_pubkey.pem)
        Issuer: C = NL, O = QuoVadis Trustlink B.V., organizationIdentifier = NTRNL-30237459, CN = QuoVadis PKIoverheid Organisatie Server CA - G3
