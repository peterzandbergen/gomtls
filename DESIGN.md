# TLS client and server design

The configuration of TLS is essential for the correct working of the 
test client and server. Therefor we elaborate on the tls settings for
server and client.

## Parameters
The server needs to set the server cert and key.

### Certificates []Certificate

Contains one or more CA chains to present to the other side for verification
of its identity. The **server** presents the chains of 
its **server** certificate. 
The **client** presents the ca chain of its **client** certificate.

Normally this array contains one Certificate. 

### RootCAs

The **client** uses these CAs Used to check the server certificate.

### ServerName

Used to verify the hostname. Mainly used by the client.

### ClientAuth

Used by the **server** to specify if and how the client should be authenticated.
mTLS.

### ClientCAs

Used by the server to verify the client certificate. 

## tls.Config for server

